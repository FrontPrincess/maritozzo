// JavaScript Document


//////////////////////////////////////////////////////////////////////////
function emptyObject(obj){
    for(var i in obj) {
        if (obj.hasOwnProperty(i)) {
            return true;
        }
    }
    return false;
}


//////////////////////////////////////////////////////////////////////////
function getUnserialize(){
    var get = {};

    if (location.search) {
        var get_array = location.search.replace(/^\?/, "").split("&");

        $.each(get_array, function (i, val) {
            var param = val.split("=");
            get[param[0]] = param[1];
        })
    }

    return get;
}


//////////////////////////////////////////////////////////////////////////
function getSerialize(get_array){

    if (emptyObject(get_array)) {
        var get = "?";

        $.each(get_array, function(i){
            get += String(i + "=" + get_array[i] + "&");
        })
        return get.slice(0, -1);
    }else{
        return "";
    }
}


////////////////////////////////////////////////////////////////
function getTransformProperty() {
    var i;
    var element = document.createElement('foo');
    var transform = [
        'transform',
        'WebkitTransform',
        //'msTransform',
        'MozTransform',
        'OTransform'
    ];
    for (i in transform) {
        if (element.style[transform[i]] !== undefined && !navigator.userAgent.match(/Opera/i)) {
            return transform[i];
        }
    }
}


////////////////////////////////////////////////////////////////
function getTransitionPrefix() {
    var i;
    var element = document.createElement('foo');

    var transform = [
        'transform',
        'WebkitTransform',
        'msTransform',
        'MozTransform',
        'OTransform'
    ];

    var prefix = [
        '',
        '-webkit-',
        '-ms-',
        '-moz-',
        '-o-'
    ];

    for (i in transform) {
        if (element.style[transform[i]] !== undefined) {
            return prefix[i];
        }
    }
}







//////////////////////////////////////////////////////////////////////////
$(function () {


    $(window).on( "orientationchange", function(e) {
        console.log('orientation');
    });

    $(window).load(function () {
        if($msg) alrt($msg);
    })




    //////////////////////////////////////////////////////////////////////////
    function hide() {
        $('#alert_container').css({'display':''})
        $('#ekran').css({'opacity':0, 'display':''});
    }

    function vis(t, callback) {
        $('#ekran').css({'opacity':0.7, 'display':'block'});
        $('#alert_container').css({'display':'block'})

        var url = '{$url}';
        $('.transfer_timer').text(t);
        var timer = window.setInterval(function () {
            t--;
            $('.transfer_timer').text(t);
            if (t == 0) {
                clearInterval(timer);
                hide();
                callback != undefined ? callback() : '';
            }
        }, 1000)

        $('#ekran').data('timer',timer);
        $('#ekran').data('callback',callback);
    }

    $('#alert_container').click(function (e) {
        clearInterval($(this).data('timer'));
        var callback = $(this).data('callback');
        hide();
        callback != undefined ? callback() : '';
    })

    $('#message_close').click(function (e) {
        clearInterval($(this).closest('#ekran').data('timer'));
        var callback = $(this).data('callback');
        hide();
        callback != undefined ? callback() : '';
    })



    function alrt(msg, callback){
        $('#alert_container').find('.alert_msg').html(msg);
        vis(5, callback);
    }



    //////////////////////////////////////////////////////////////////////////
/*    $('#main').delegate('.online, .offline', 'click', function(e){
        e.preventDefault();

        var url = e.currentTarget.tagName == 'A' ? e.currentTarget.getAttribute("href") : "/category/collection";
        //var url = "/category/collection";

        $.ajax({
            async : false,
            type: 'POST',
            url: url,
            dataType: 'json',
            data: "oper_type=foo&online=" + ($(this).is('.online') ? 1 : 0),
            cache: false,
            success:function (response) {
                location.assign(url);
            }
        });

    })*/






    //////////////////////////////////////////////////////////////////////////
    $('#logout').click(function(e){
        e.preventDefault();

        $.ajax({
            async : false,
            type: 'GET',
            url: "/logout",
            dataType: 'json',
            cache: false,
            success:function (response) {
                location.assign("/");
            }
        });

    });



    //////////////////////////////////////////////////////////////////////////
    $('#main').delegate('.error', 'click', function(e){
        $(this).removeClass('error');
    })

    $('.order>a').click(function(e){
        e.preventDefault();
        var match = /([\d\S]+)\[([\d]*)\]$/.exec($(this).attr('name'));
        var form = $(this).closest('form');

        $.ajax({
            async : false,
            type: 'POST',
            url: "/order",
            dataType: 'json',
            data: $(this).closest('form').serialize() + "&oper_type="+match[1] + "&delivery_method=2",
            cache: false,
            success:function (response) {
                if(response.status){
                    location.assign('/order-confirm');
                } else {
                    var error = '<h3>Ошибки заполнения формы:</h3>';
                    $.each(response.error, function (i, e) {
                        form.find('[name=' + i + ']').addClass('error');
                        error += e + '<br>';
                    })
                    alrt(error);
                }
            }
        });
    });



    //////////////////////////////////////////////////////////////////////////
    $('.orderconfirm>a').click(function(e){
        e.preventDefault();
        var match = /([\d\S]+)\[([\d]*)\]$/.exec($(this).attr('name'));

        $.ajax({
            async : false,
            type: 'POST',
            url: "/order",
            dataType: 'json',
            data: "oper_type="+match[1],
            cache: false,
            success:function (response) {
                if (response.status) {
                    alrt(response.msg, function (e) {
                        if(payment_type == 3){
                            location.assign('/payment');
                        }else{
                            location.assign('/category/catalog');
                        }
                    });
                } else {
                    alrt(response.msg, function (e) {
                        location.assign('/order-confirm');
                    });
                }
            }
        });

    });



    //////////////////////////////////////////////////////////////////////////
    $('.sendSMS>a, .phoneConfirm>a').click(function(e){
        e.preventDefault();

        $sms = $("input[name=sms]").length = 0 ? '' : $("input[name=sms]").val();

        $.ajax({
            async:false,
            type:'PUT',
            url: $(this).attr('href'),
            dataType:'json',
            data: "oper_type=" + $(this).attr('data-oper_type') + "&phone=" + $("input[name=phone]").val() + "&sms=" + $sms,
            success:function (response) {
                if (response.status) {
                    alrt(response.msg, function (e) {
                        location.reload();
                    });
                } else {
                    alrt(response.msg);
                }
            }
        });
    })



    //////////////////////////////////////////////////////////////////////////
    $('.delete>a, .add>a, .del>a, .plus>a, .minus>a').click(function (e) {
        e.preventDefault();
        var match = /\[([\S]*)\]\[([\S]*)\]/.exec($(this).attr('data-id'));
        var id = match[1];
        var variant_id = match[2];
        var oper_type = $(this).attr('data-oper_type');
        //console.log(match);

        $.ajax({
            async : false,
            type: 'POST',
            url: "/cart",
            dataType: 'json',
            data:"oper_type=" + oper_type + "&product_id=" + id  + "&variant_id=" + variant_id + "&" + $(this).closest('form').serialize(),
            cache: false,
            success:function (response) {
                if (response.status){
                    if (response.msg) {
                        alrt(response.msg, function (e) {
                            location.reload();
                        })
                    } else {
                        location.reload();
                    }
                }
            }
        });
    })



    //////////////////////////////////////////////////////////////////////////
    $('.buy>a').click(function(e){
        e.preventDefault();
        //var match = /([\d\S]+)\[([\d]*)\]$/.exec($(this).attr('data-name'));

        //if ($('select.size option:selected').val() || $('select.size').length == 0) {

            $.ajax({
                async:false,
                type:'POST',
                url:"/cart",
                dataType:'json',
                data:"oper_type=buy&product_id=" + $(this).attr('data-id') + "&" + $(this).closest('form').serialize(),
                cache:false,
                success:function (response) {
                    if (response.status){
                        //location.reload();
                        $('#cart_block').html(response.data);
                        $('.cart').stop();
                        $('.cart').css({'display':'block'}).animate({'opacity':1}, 1000);
                        var timer = setTimeout(function () {
                            $('.cart').stop();
                            $('.cart').animate({'opacity':0}, 1500, function (e) {
                                $('.cart').css({'display':'none'})
                            });
                        }, 5000);

                        $('.cart').data('timer',timer);
                    }else{
                        alrt(response.msg, function (e) {
                            location.reload();
                        })
                    }
                }
            });

        //}else{
        //    $(".size_alert").css({'display':''});
        //}

    })




    //////////////////////////////////////////////////////////////////////////
    $(':submit.aut').click(function (e) {
        e.preventDefault();

        $.ajax({
            async:false,
            type:'PUT',
            url:"/user",
            dataType:'json',
            data:"oper_type=aut&" + $(this).closest('form').serialize(),
            cache:false,
            success:function (response) {
                if (!response.status && response.msg) {
                    alrt(response.msg, function () {
                        if (response.status) location.reload();
                    });
                }else{
                    location.reload();
                }
            }
        });
    })




    //////////////////////////////////////////////////////////////////////////
    $('.resetpassword').click(function (e) {
        e.preventDefault();

        if ($(this).closest('form').find('input[name=login]').val()) {
            $.ajax({
                async:false,
                type:'PUT',
                url:"/user",
                dataType:'json',
                data:"oper_type=mailResetPassword&" + $(this).closest('form').serialize(),
                cache:false,
                success:function (response) {
                    if (response.status) {
                        if (response.msg) alrt(response.msg, function () {
                            location.reload();
                        });
                    } else {
                        if (response.msg) alrt(response.msg);
                    }
                }
            });
        } else {
            $(this).closest('form').find('input[name=login]').addClass('error');
        }

    })




    //////////////////////////////////////////////////////////////////////////
    $('.updateMainParam>a, .updateAddress>a').click(function (e) {
        e.preventDefault();

        $.ajax({
            async:false,
            type:'PUT',
            url:$(this).attr('href'),
            dataType:'json',
            data:"oper_type=" + $(this).attr('data-oper_type') + "&" + $(this).closest('form').serialize(),
            cache:false,
            success:function (response) {
                alrt(response.msg, function(){
                    if(response.error){
                        $.each(response.error, function(i,o){
                            $("input[name=" + i + "]").addClass('error');
                            $("textarea[name=" + i + "]").addClass('error');
                        })
                    }else{
                        location.href = "/cabinet#" + $(e.target).closest('.block').attr('id');
                        location.reload();
                    }
                });
            }
        });

    })



    //////////////////////////////////////////////////////////////////////////
    $('.updatePassword>a').click(function (e) {
        e.preventDefault();

        $.ajax({
            async:false,
            type:'PUT',
            url:$(this).attr('href'),
            dataType:'json',
            data:"oper_type=updatePassword&" + $(this).closest('form').serialize(),
            cache:false,
            success:function (response) {
                alrt(response.msg, function(){
                    if(response.error){
                        $.each(response.error, function(i,o){
                            $("input[name=" + i + "]").addClass('error');
                            $("textarea[name=" + i + "]").addClass('error');
                        })
                    }else{
                        location.href = "/cabinet#user_main";
                        location.reload();
                    }
                });
            }
        });

    })



    //////////////////////////////////////////////////////////////////////////
    $('.deleteAddress>a').click(function (e) {
        e.preventDefault();

        $.ajax({
            async:false,
            type:'PUT',
            url:$(this).attr('href'),
            dataType:'json',
            data:"oper_type=" + $(this).attr('data-oper_type') + "&id=" + $(this).attr('data-id'),
            cache:false,
            success:function (response) {
                alrt(response.msg, function () {
                    location.href = "/cabinet#user_address";
                    location.reload();
                });
            }
        });
    })



    //////////////////////////////////////////////////////////////////////////
    $('.addAddress>a').click(function (e) {
        e.preventDefault();

        if ($('#add_address').css('display') == 'none') {
            $('#add_address').css('display','');
            $('.updateAddress').css('display', 'none');
        } else {
            $.ajax({
                async:false,
                type:'PUT',
                url:$(this).attr('href'),
                dataType:'json',
                data:"oper_type=" + $(this).attr('data-oper_type') + "&" + $(this).closest('form').serialize(),
                cache:false,
                success:function (response) {
                    alrt(response.msg, function () {
                        if (response.error) {
                            $.each(response.error, function (i, o) {
                                $("input[name=" + i + "]").addClass('error');
                                $("textarea[name=" + i + "]").addClass('error');
                            })
                        } else {
                            location.href = "/cabinet#user_address";
                            location.reload();
                        }
                    });
                }
            });
        }

    })




    //////////////////////////////////////////////////////////////////////////
/*    $(':submit.reg').click(function (e) {
        e.preventDefault();
        var input = $(this).closest('form').find("input[name='email']");

        $.ajax({
            async:false,
            type:'POST',
            url:"/",
            dataType:'json',
            data:"ajax=true&opertype=reg&" + $(this).closest('form').serialize() + '&url=' + location.pathname,
            cache:false,
            success:function (response) {
                if (response.msg) alrt(response.msg, function () {
                    if (response.status) {
                        location.reload();
                    } else {
                        input.addClass('error');
                    }
                });
            }
        });
    })*/



    $(':submit.mailConfirm').click(function (e) {
        e.preventDefault();
        //var input = $(this).closest('form').find("input[name='email']");

        $.ajax({
            async:false,
            type:'POST',
            url:"/user",
            dataType:'json',
            data:"oper_type=mailConfirm&" + $(this).closest('form').serialize() + '&url=' + location.pathname,
            cache:false,
            success:function (response) {

                var error = '<br/><br/>';

                if(response.error){
                    $.each(response.error, function (i, o) {
                        error += o + '<br/><br/>';
                    })
                }

                
                if (response.msg) alrt(response.msg + error, function () {
                    if (response.status) {
                        location.reload();
                    } else {
                        //input.addClass('error');
                    }
                });
            }

        });
    })




    $("#promo").click(function(e){
        e.preventDefault();


        var form = $(this).closest('form');

        $.ajax({
            async : false,
            type: 'POST',
            url: "/order",
            dataType: 'json',
            data: $(this).closest('form').serialize() + "&oper_type=checkOrder&delivery_method=2",
            cache: false,
            success:function (response) {
            }
        });


        $.ajax({
            async : false,
            type: 'POST',
            url: "/promo",
            dataType: 'json',
            data: 'oper_type=activated&code=' + $("input[name=promo]").val(),
            cache: false,
            success:function (response) {
                if(response.status){
                    alrt(response.msg, function(){location.assign('/userorder')});
                } else {
                    error = 'Ошибка активации<br/>';
                    $.each(response.error, function (i, e) {
                        error += e + '<br/>';
                    })
                    alrt(error);
                }
            }
        });

    })




})