// JavaScript Document

$(function () {

    $(window).load(function () {
    });


    /*-------------------------------------------------------------*/
    


    /*-------------------------------------------------------------*/

    $('#main').delegate('#cart', 'mouseover', function (e) {
        clearTimeout($('.cart').data('timer'));

        $('.cart').stop();
        $('.cart').css({'display':'block'}).animate({'opacity':1});
    })


    $('#main').delegate('#cart', 'touchstart', function (e) {
        $('.cart').stop();
        $('.cart').css({'display':'block'}).animate({'opacity':1});

        setTimeout(function () {
            $('.cart').stop();
            $('.cart').animate({'opacity':0}, 1500, function (e) {
                $('.cart').css({'display':'none'})
            });
        }, 3000);
    })




    $('#main').delegate('.cart', 'mouseover', function (e) {
        clearTimeout($(this).data('timer'));
    })


    $('#main').delegate('.cart', 'mouseleave', function (e) {
        var this_ = $(this);

        var timer = setTimeout(function () {
            $('.cart').stop();
            this_.animate({'opacity':0}, function(e){
                this_.css({'display':'none'})
            });
        }, 1000);

        $(this).data('timer',timer);
    })




    /*-------------------------------------------------------------*/

    $('#main').delegate('#return_top', 'click', function(e){
        if($.browser.safari){
            $('body').animate( { scrollTop: 0 }, 500 );
        }else{
            $('html').animate( { scrollTop: 0 }, 500 );
        }
    })




    $('#main').delegate('input[name="phone"]', 'keypress', function(e){
        var key = e.charCode || e.keyCode || 0;
        console.log(key);
        return (key == 8 || key == 9 || key == 37 || key == 39 || key == 46 || (key >= 48 && key <= 57));
    })
    $('#main').delegate('input[name="phone"]', 'change', function(e){
        $(this).val($(this).val().replace(/[^\d]/gi,''));
    })



})
